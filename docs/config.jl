using IgaBase

package_info = Dict(
    "modules" => [IgaBase],
    "authors" => "Rene Hiemstra and contributors",
    "name" => "IgaBase.jl",
    "repo" => "https://gitlab.com/feather-ecosystem/core/IgaBase",
    "pages" => [
        "About"  =>  "index.md"
        "API"  =>  "api.md"
    ],
)

DocMeta.setdocmeta!(IgaBase, :DocTestSetup, :(using IgaBase); recursive=true)

# if docs are built for deployment, fix the doctests
# which fail due to round off errors...
if haskey(ENV, "DOC_TEST_DEPLOY") && ENV["DOC_TEST_DEPLOY"] == "yes"
    doctest(IgaBase, fix=true)
end
